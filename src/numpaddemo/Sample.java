/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numpaddemo;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Sadanand.rudraiah@crudsinfotech.com
 */
public class Sample extends Application {
    
    private double xOffset = 0;
    private double yOffset = 0;  

    @Override
    public void start(Stage primaryStage){
        try {
            BorderPane root = new BorderPane();
            Scene scene = new Scene(root,400,400);

            Button bSecondStage = new Button("Show second Stage");
            bSecondStage.setOnAction(evt -> {
                
               // FXMLLoader loader = new FXMLLoader((getClass().getResource("/numpaddemo/NumPad.fxml")));
                FXMLLoader loader = new FXMLLoader((getClass().getResource("/numpaddemo/KeyPad.fxml")));
                
                KeyPadController controller = null;
                Parent popRoot = null;
                try {
                     popRoot = loader.load();
                    controller = loader.getController();
                Scene scene2 = new Scene(popRoot);
                Stage stage2 = new Stage();

                stage2.initModality(Modality.APPLICATION_MODAL);
                stage2.setTitle("Add Bill");
                //stage.getIcons().add(new Image("/images/logo.jpg"));
                stage2.initStyle(StageStyle.UNDECORATED);
                stage2.setScene(scene2);
                stage2.showAndWait();
                    System.out.println("Amount Entered ==>>" + controller.getKeyboardInput()); 
                     
                    //WindowController wc = new WindowController();
                    //wc.showStage();
                    //System.out.println(wc.getData());
                } catch (IOException ex) {
                    Logger.getLogger(Sample.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            root.setCenter(bSecondStage);


            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        launch(args);
    }    
    
   class WindowController {
        private String data;

        void showStage() {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);

            VBox root = new VBox();
            Scene scene = new Scene(root);
            TextField tf = new TextField();
            Button submit = new Button("Submit");

            submit.setOnAction(e -> {
                data = tf.getText();
                stage.close();
            });

            root.getChildren().addAll(tf, submit);
            stage.setScene(scene);
            stage.showAndWait();
        }

        String getData() {
            return data;
        }
    }    
    
}
