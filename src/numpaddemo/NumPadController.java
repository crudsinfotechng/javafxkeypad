/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numpaddemo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Sadanand.rudraiah@crudsinfotech.com
 */
public class NumPadController implements Initializable {

    
    @FXML
    private TextField txtAmt;
    
    private String enteredAmount;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    
    public String getCalcAmount()
    {
        return enteredAmount;
    }

  @FXML
    void handleButtonClick(ActionEvent event) {
        //System.out.println("Button pressed " + ((Button) event.getSource()).getText());
        String data = ((Button) event.getSource()).getText();
        
        if(data.equals("Clear"))
        {
            txtAmt.clear();
        }
        else
        {
            txtAmt.setText(txtAmt.getText() + data);
        }
        
    }

    
    @FXML
    void handleOK(ActionEvent event) {
        enteredAmount = txtAmt.getText();
        //System.out.println("****" + enteredAmount);
        Stage stg = (Stage) ((Node) (event.getSource())).getScene().getWindow();
        stg.close();
    }    
}
