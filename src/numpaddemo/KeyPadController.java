/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numpaddemo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Sadanand.rudraiah@crudsinfotech.com
 */
public class KeyPadController implements Initializable {
    
    @FXML
    private TextField txtInput;
    
    private String enteredText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public String getKeyboardInput()
    {
        return enteredText;
    }
    
  @FXML
    void handleButtonClick(ActionEvent event) {
        //System.out.println("Button pressed " + ((Button) event.getSource()).getText());
        String data = ((Button) event.getSource()).getText();
        
        if(data.equals("Del"))
        {
            txtInput.clear();
        }
        else if(data.equals("Space"))
        {
           txtInput.setText(txtInput.getText() + " "); 
        }
        else
        {
            txtInput.setText(txtInput.getText() + data);
        }
        
    }    


    @FXML
    void handleOK(ActionEvent event) {
        enteredText = txtInput.getText();
        Stage stg = (Stage) ((Node) (event.getSource())).getScene().getWindow();
        stg.close();
    }    
    
}
